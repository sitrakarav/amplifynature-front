import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

admin.initializeApp(functions.config().firebase);


// exports.countExtraitsEnjeu = functions.database.ref("/phrases_csv/{csvId}/{phraseId}/enjeu").onCreate((snapshot, context) => {

//     const enjeu = snapshot.val();
//     const csvPhrase = context.params.csvId;
//  	console.log("on create here ");

//  	if(enjeu && enjeu.replace(/#/g, '') == ""){
//  		return null;
//  	}
//  	const ref = admin.database().ref("enjeux_extraits/"+enjeu.replace(/#/g, '')+"/"+csvPhrase);
//  	return ref.transaction(current => {
//         return (current || 0) + 1;
//     });
// });

exports.updateCountExtraitsEnjeu = functions.database.ref("/phrases_csv/{csvId}/{phraseId}/enjeu").onUpdate((change, context) => {

    
    const old_enjeu = change.before.val();
    const csvPhrase = context.params.csvId;
 
    // Exit when the data is deleted.
    

	 	
 	
 	let promises = [];
	 	

    if((change.after.val() != change.before.val())) {

    	const enjeu = change.after.val();
        console.log("csv Phrase , phrase, old_enjeu %o, new enjeu ",csvPhrase, context.params.phraseId, old_enjeu, enjeu);
    	if(enjeu && enjeu.replace(/#/g, '') == ""){
	 		return null;
	 	}

	 	if(enjeu && old_enjeu.replace(/#/g, '') == ""){
	 		return null;
	 	}

    	const ref = admin.database().ref("enjeux_extraits/"+enjeu.replace(/#/g, '')+"/"+csvPhrase);
    	promises.push(ref.transaction(current => {
	        return (current || 0) + 1;
	    }));

	    const old_ref = admin.database().ref("enjeux_extraits/"+old_enjeu.replace(/#/g, '')+"/"+csvPhrase);
		promises.push(old_ref.transaction(current => {
	        return (current && current > 0) ? current - 1 : 0;
	    }));
    }

    
 	
 	return Promise.all(promises);
});

exports.deleteCountExtraitsEnjeu = functions.database.ref("/phrases_csv/{csvId}/{phraseId}/enjeu").onDelete((snapshot, context) => {

    const enjeu = snapshot.val();
    const csvPhrase = context.params.csvId;
    return admin.database().ref("enjeux_extraits/"+enjeu.replace(/#/g, '')+"/"+csvPhrase).remove();

});



