// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    // apiKey: "AIzaSyBQbdFemFRh0uJqqgfFdRgcmh3wO7TU0J4",
    // authDomain: "amplifynature-faa20.firebaseapp.com",
    // databaseURL: "https://amplifynature-faa20.firebaseio.com",
    // projectId: "amplifynature-faa20",
    // storageBucket: "amplifynature-faa20.appspot.com",
    // messagingSenderId: "1030922528498",
    // appId: "1:1030922528498:web:dc4bd1c3f509e5e7"

    apiKey: 'AIzaSyD9iGZ8J8KoOB6eBcXSTmnZjhFGemeHcTk',
    authDomain: 'amplifynature-8be2d.firebaseapp.com',
    databaseURL: 'https://amplifynature-8be2d.firebaseio.com',
    projectId: 'amplifynature-8be2d',
    storageBucket: 'amplifynature-8be2d.appspot.com',
    messagingSenderId: '459285736048'

    // apiKey: "AIzaSyD3DsyKkd2Z9Vhzo_OqnxjtLwTvmSXcAmQ",
    // authDomain: "amplify-nature.firebaseapp.com",
    // databaseURL: "https://amplify-nature.firebaseio.com",
    // projectId: "amplify-nature",
    // storageBucket: "amplify-nature.appspot.com",
    // messagingSenderId: "790534504456"

    // apiKey: "AIzaSyAyI_EvICRruCLVH-hwJCjPugEpWcoMXoo",
    // authDomain: "amplify-nature-2.firebaseapp.com",
    // databaseURL: "https://amplify-nature-2.firebaseio.com",
    // projectId: "amplify-nature-2",
    // storageBucket: "amplify-nature-2.appspot.com",
    // messagingSenderId: "178005779326"



  },
  mapbox: {
    accessToken: 'pk.eyJ1IjoidHNpb3giLCJhIjoiY2p0NGV1Z3dlMTBjbzQzcGdrbjRjNmU1NSJ9.Dsvl1gPtmb3lxNSaK5x7ZQ'
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
