
import { Deserializable } from "./deserializable.model";

export class MotEnjeu implements Deserializable{

	mot:string;
	point:number;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}
