
import { Deserializable } from "./deserializable.model";

export class Flux implements Deserializable{

	id:string;
	flux:string;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}
