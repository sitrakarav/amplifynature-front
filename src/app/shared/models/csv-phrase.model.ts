
import { Deserializable } from "./deserializable.model";
import { Phrase } from "./phrase.model";

export class CsvPhrase implements Deserializable{

	nom:string;
	nom_pdf:string;
	pdf_url:string;
	titre_fr:string;
	titre_en:string;
	auteur:string;
	date:string;
	pays:string;
	lat:number;
	long:number;
	extraits:number;
	phrases:Phrase[];
	valid:boolean = false;
	active:boolean = false;
	csv:string;
	flux:string;
	filename:string;
	url:string;
	sat:any[];
	enjeux:any[];
	
	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}

