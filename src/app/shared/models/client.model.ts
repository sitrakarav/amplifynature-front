
import { Deserializable } from "./deserializable.model";
import { Flux } from "./flux.model";

export class Client implements Deserializable{

	key:string;
	num:string;
	prenom:string;
	nom:string;
	mail:string;
	organisation:string;
	poste:string;
	langue:string;
	password:string;
	last_connection:string;
	orga_img_url:string;
	orga_png:string;
	img_url:string;
	client_png:string;
	favicon_png:string;
	frontoffice_png:string;
	flux:Flux[];

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}
