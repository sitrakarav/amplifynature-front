import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../router.animations';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { ActivatedRoute, ParamMap } from '@angular/router';
    

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    email: string;
    password: string;
    error: string= "";
    loading:Boolean = false;
    loginPage:Boolean = true;
    clientId:any = false;
    client:any = false;
    loginStyle:any = {};
    background:any = "";
    constructor(
        private translate: TranslateService,
        public router: Router,
        public route: ActivatedRoute,
        private auth: AngularFireAuth,
        private db: AngularFireDatabase
        ) {
            this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
            this.translate.setDefaultLang('fr');
            const browserLang = this.translate.getBrowserLang();
            this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'fr');
    }

    ngOnInit() {
        let self = this;
        this.clientId = this.route.snapshot.paramMap.get("client");
        this.background = '';
        if(this.clientId){
            this.db.object("clients/"+this.clientId).snapshotChanges().subscribe(function(snapshot:any){
                if(snapshot.payload.val()){
                    self.client = snapshot.payload.val();
                    let client = snapshot.payload.val();
                    let link = (document.querySelector("link[rel*='icon']") || document.createElement('link')) as HTMLLinkElement;
                    link.type = 'image/png';
                    link.rel = 'shortcut icon';
                    link.href = client.favicon_img_url+"?v=2";

                    window.document.getElementsByTagName('head')[0].appendChild(link);
                    console.log("changing favicon %o %o",client.favicon_img_url, window.document.getElementsByTagName('head')[0])
                    // console.log("background %o", client.frontoffice_img_url);
                    if(client.frontoffice_img_url.includes("http")){
                        console.log("background edit %o", client.frontoffice_img_url);

                        self.background = client.frontoffice_img_url;
                        console.log("STYLE %o", self.loginStyle);
                    }    
                    
                }
            }) 
        }else{
            this.db.database.ref("clients").once("value",function(data){
                for(let i in data.val()){
                    let user  = data.val()[i];
                    if(user.favicon_img_url){
                        let link = (document.querySelector("link[rel*='icon']") || document.createElement('link')) as HTMLLinkElement;
                        console.log("first link %o", link);
                        link.type = 'image/svg+xml';
                        link.rel = 'shortcut icon';
                        link.href = user.favicon_img_url+"?v=8989";
                        console.log("Changing favicon to %o", link.href);

                        window.document.getElementsByTagName('head')[0].appendChild(link);
                        break;
                    }
                    
                }
            });
        }


    }

    onLoggedin() {
        let self = this;
        this.loading = true;
        console.log(this.email );
        console.log(this.password );
        let data = new Date().getDate()+"/"+new Date().getMonth()+"/"+new Date().getFullYear();
        if(this.email && this.password){
            this.auth.auth.signInWithEmailAndPassword(this.email, this.password).then(function(user){
                self.error = "";
                self.db.database.ref("clients").orderByChild("mail").equalTo(self.email).once("value", function(snapshot){      
                        console.log("snapshot : ",snapshot.val());
                    if(snapshot.val()){
                        for(let i in snapshot.val()){
                            self.db.database.ref("clients/"+snapshot.val()[i].num+"/last_connection").set(data);
                            localStorage.setItem('user', JSON.stringify(snapshot.val()[i]));
                            localStorage.setItem('isLoggedin', 'true');

                            self.router.navigate(['/home']);
                            console.log("user signed %o", user);
                            self.loading = false;
                            break;
                        }
                    }else{
                        self.router.navigate(['/login']);
                    }
                   
                    
                })
                
                            
            }).catch(function(error) {
                  self.error = error;
                  self.loading = false;
            });
        }else{
            this.loading = false;
        }
    }

    forgotPassword(){
        this.loginPage = false;
    }
    login(){
        this.loginPage = true;
    }

    resetPassword(){
        let self = this;
        if(this.email){
            this.loading = true;
            this.auth.auth.sendPasswordResetEmail(this.email).then(function(){
                self.error = "";
                self.loginPage = true;
                self.loading = false;
            }).catch(function(error) {
                  self.error = error;
                  self.loading = false;
            });
        }
    }
}
