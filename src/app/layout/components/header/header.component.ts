import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Client } from '../../../shared/models/client.model';
import * as jQuery from 'jquery';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public pushRightClass: string;

    public user: any = {};
    closeResult: string;
    last_conn: any[] = [];
    hide_profil: boolean = false;
    hide_profil1: boolean = false;
    hide_profil2: boolean = false;
    hide_profil3: boolean = false;
    traduction: any = {};

    constructor(private translate: TranslateService, public router: Router,
        private db: AngularFireDatabase,
        private auth: AngularFireAuth,
        private modalService: NgbModal
        ) {

        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        let user : Client = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
        if (user) {
            this.translate.use(user.langue.toLowerCase());
        }
        else {
            const browserLang = this.translate.getBrowserLang();
            this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
        }

        // console.log("USER %o", user);


        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit(){
        let self = this;
        this.user = JSON.parse(localStorage.getItem('user'));


            let link = (document.querySelector("link[rel*='icon']") || document.createElement('link')) as HTMLLinkElement;
            // link.type = 'image/x-icon';
            // link.rel = 'shortcut icon';
            // link.href = this.user.favicon_img_url+'?='+Math.random();
            // window.document.getElementsByTagName('head')[0].appendChild(link);

            // let link = document.createElement('link') as HTMLLinkElement;
            // let  oldLink = document.getElementById('dynamic-favicon');
            link.id = 'dynamic-favicon';
            link.rel = 'icon';
            link.type = 'image/x-icon';
            // link.href = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9oFFAADATTAuQQAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAAEklEQVQ4y2NgGAWjYBSMAggAAAQQAAGFP6pyAAAAAElFTkSuQmCC";
            link.href = this.user.favicon_img_url;
            window.document.getElementsByTagName('head')[0].appendChild(link);
            // if (oldLink) {
            //   document.head.removeChild(oldLink);
            // }
            // document.head.appendChild(link);

            // this.convertImgToBase64URL(this.user.favicon_img_url, function(base64Img){
            //     link.href = base64Img;
            //     window.document.getElementsByTagName('head')[0].appendChild(link);
            // });
            // console.log("url %o", link.href);
            // console.log("link 3 %o", window.document.getElementsByTagName('head')[0])

         if(!this.user){
             this.router.navigate(['/login']);
         }
        self.last_conn = [];
        this.db.database.ref("clients").once("value",function(data){
            for(let i in data.val()){
                let user  = data.val()[i];
                if(user.favicon_img_url){

                }
                break;
            }
            for(let i in data.val()){

                if(self.user.num != data.val()[i].num && self.user.organisation == data.val()[i].organisation){
                    self.last_conn.push(data.val()[i]);
                }
            }
            //let dateA = 0;
            //let dateB = 0;
            self.last_conn = self.last_conn.map((a) =>{
                a.last_connection = (a.last_connection.split("/")[0])+"/"+(parseInt(a.last_connection.split("/")[1])+1)+"/"+(a.last_connection.split("/")[2]);
                return a;
            });
            self.last_conn = self.last_conn.sort(function compare(a, b) {


                //if(a.last_connection.split("/").length == 3 && b.last_connection.split("/").length == 3)
                //{

                    let dateA = +new Date(((a.last_connection.split("/")[0])+(a.last_connection.split("/")[1])+(a.last_connection.split("/")[2])));
                    let dateB = +new Date(((b.last_connection.split("/")[0])+(b.last_connection.split("/")[1])+(b.last_connection.split("/")[2])));
                    //dateA = +new Date(a.last_connection) - +new Date(b.last_connection);
                //}
                //let res = dateA - dateB;
                //console.log("le res: "+res);
                return dateA - dateB;
            });


            // console.log("self.last_conn %o",self.last_conn);

        });

        this.db.database.ref("trad").on('value', function(data){
            data.forEach(function(child){
              self.traduction[child.val().ref] = child.val();
            });
        });

    }



     ngAfterViewInit() {
        let self = this;
        this.pushRightClass = 'push-right';



        // if(!this.auth.auth.currentUser){
        //     self.router.navigate(['/login']);
        // }else{
        //     self.db.database.ref("clients").orderByChild("mail").equalTo(self.auth.auth.currentUser.email).once("value", function(snapshot){
        //             if(snapshot.val()){
        //                 for(let i in snapshot.val()){
        //                     self.user = snapshot.val()[i];
        //                     break;
        //                 }
        //             }else{
        //                 self.router.navigate(['/login']);
        //             }


        //         })
        // }
        // this.auth.auth.onAuthStateChanged(function(user){
        //     if(user){
        //         self.db.database.ref("clients").orderByChild("mail").equalTo(self.auth.auth.currentUser.email).once("value", function(snapshot){
        //             if(snapshot.val()){
        //                 for(let i in snapshot.val()){
        //                     self.user = snapshot.val()[i];
        //                     break;
        //                 }
        //             }else{
        //                 self.router.navigate(['/login']);
        //             }


        //         })
        //     }else{
        //         self.router.navigate(['/login']);
        //     }
        // })


    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout(content) {
        this.open(content);
    }

    changeLang(language: string) {
        let user:Client = JSON.parse(localStorage.getItem("user"));
        user.langue = language.toLowerCase();
        localStorage.setItem("user", JSON.stringify(user));
        // window.location.reload();
        this.translate.use(language);
    }

    open(content) {
        this.modalService.open(content, {centered:true}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    deconnexion(){
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('user');
    }

    checkProfil(){

       this.hide_profil = this.hide_profil ? false : true;

    }

    checkProfil1(){
        this.hide_profil1 = true;

    }

    checkProfil2(){
        this.hide_profil2 = true;

    }

    checkProfil3(){
        this.hide_profil3 = true;

    }

    leaveProfil1(){
        this.hide_profil1 = false;

    }

    leaveProfil2(){
        this.hide_profil2 = false;
    }

    leaveProfil3(){
        this.hide_profil3 = false;
    }

    lang(mot){
        return this.traduction[mot] != undefined ? this.traduction[mot][this.translate.currentLang] : mot;
    }

}
