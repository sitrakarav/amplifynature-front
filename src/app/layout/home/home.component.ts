import { environment } from '../../../environments/environment';
import * as mapboxgl from 'mapbox-gl';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { Enjeu } from "../../shared/models/enjeu.model";
import { Phrase } from "../../shared/models/phrase.model";
import { CsvPhrase } from "../../shared/models/csv-phrase.model";
import { Client } from "../../shared/models/client.model";

import * as jQuery from "jquery";

import { Component, Output, EventEmitter, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as echarts from 'echarts';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { Options, ChangeContext, PointerType  } from 'ng5-slider';
// import { BrowserModule } from '@angular/platform-browser';
// import { FormsModule } from '@angular/forms';
// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';




@Component({
	//encapsulation: ViewEncapsulation.None,
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	value: number = 0;
    options: Options = {};
    highValue : number = 0;

	enjeux: Enjeu[];
	allEnjeux: Enjeu[];
	selectedEnjeux: Enjeu[] = [];
	enjeuxExtraits: any[] = [];
	enjeu:Enjeu;
	enjeuOver:Enjeu;
	enjeuInfo:Enjeu;
	seuil:number =0;

	csvPhrases: CsvPhrase[];
	csvPhrasesFiltered: CsvPhrase[];
	csvPhrase: CsvPhrase = new CsvPhrase();
	csvPhrase2: CsvPhrase = new CsvPhrase();
	csvSelected:string;

	phrases:any = {};

	loading: Boolean = false;
	loadingMap: Boolean = false;
	traduction:any = {};
	couleurs:any = {};
	dates:string[] = [];
	years:string[] = [];
	pays:string[] = [];
	paysFilter:string = "all";

	couleurPhrase:any = {};
	flux:any[] = [];
	allFlux:any[] = [];
	fluxFilter:any[] = [];
	perfFilter:any[] = [];

	extraits:any[] = [];

	allFluxFilter:Boolean = true;
	allPerf:Boolean = true;
	fluxLimit:Number = 3;

	yearFilter:any = false;
	month_from:string;
	year_from:string;

	month_to:string;
	year_to:string;

	fluxData:any[] = [];
	csvPhrasesData:any[] = [];


	mapToogle: boolean = true;
	collapedSideBar: boolean;

	isActive: boolean;
	isActive2: boolean;
	isActive3:boolean;
    collapsed: boolean;
    collapsed2: boolean ;
    collapsed3: boolean ;
    showMenu: string;
    pushRightClass: string;
    pushLeftClass: string;

    client:Client = new Client();

    chartOptions:any;

    closeResult: string;

    user:any = {};

    sideEnjeux:boolean = false;
    client_enjeu:Enjeu[];
    titre_fr:any[]=[];
    titre_en:any[]=[];
    phrase_en:any[]=[];
    phrase_fr:any[]=[];
    mot_cle:any[]=[];
    countEnjeu:any;
    motEntree:string = "";
    allPaysFilter:Boolean= true;
    newpaysFilter:any[]=[];
    filter_enjeu:any[]=[];
    filter:any;
    hide_map:boolean = false;
    hide_graph:boolean = true;
    showFilterDate:boolean = false;
    showSliderDate:boolean = false;
    showRangeDate:boolean = false;
    csvClick:boolean=false;
    next:number;
	showCheck:boolean = false;
	liste_pays:any[] = [];
	all_pays:string="Indifférent";
	dateFilterToggle : boolean;
	showSlider : boolean = true;
	test: boolean = false;
	test2 : boolean = false;

	countExtraits:any[] = [];

    @Output() collapsedEvent = new EventEmitter<boolean>();


  	constructor(
  		private translate: TranslateService, public router: Router,
  		private db: AngularFireDatabase,
  		private auth: AngularFireAuth,
  		private modalService: NgbModal

  	) {
  		this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('fr');
        // const browserLang = this.translate.getBrowserLang();
        // this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'fr');
        let user:Client = JSON.parse(localStorage.getItem('user'));
        this.translate.use(user.langue.toLowerCase());
        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });


        this.csvPhrase.phrases = [];
        this.couleurPhrase = {"0": "#eee","1": "#D8F0BC","2": "#AEE571","3": "#7CB342","4": "#4B830D"};
  	 }

	ngOnInit() {
	  	let self = this;

	  	this.user = JSON.parse(localStorage.getItem('user'));



        jQuery(".main-container").css("margin-left", "0");



	  	this.loading = true;
	  	this.loadingMap = true;
	  	this.db.database.ref("trad").once('value', function(data){
	            data.forEach(function(child){
	              self.traduction[child.val().ref.toLowerCase().replace(/\s+$/, '')] = child.val();
	            });
	            // console.log("self.traduction %o", self.traduction)

	      });
	  	this.db.database.ref("couleur").once('value', function(data){
	            // self.couleurs = data.val();
	            for(let i in data.val()){
	            	self.couleurs[data.val()[i].objet.toLowerCase().trim()] = data.val()[i].couleur;
	            }
	            self.couleurPhrase = {"0": "#eee","1": self.getColor('très négatif'),"2": self.getColor('déceptif'),"3": self.getColor('encourageant'),"4": self.getColor('très positif'), "-1" : self.getColor('Objectif non traité') };

	     })

	  	// this.db.database.ref("enjeux_extraits").once('value', function(data){

	   //      self.enjeuxExtraits = data.val();

	   //   })
	  	// this.db.database.ref("enjeux_extraits").once('value', function(data){
	  	// 	// for(let i in data.val()){
	  	// 	// 	console.log(i);
	  	// 	// 	console.log(data.val()[i]); 
	  			
	  	// 	// }
	  	// 	data.forEach(function(child){
	  	// 		let count = 0;
	  	// 		//console.log("ID",child.key);
	  	// 		for(let i in child.val()){
	  	// 			count = count + child.val()[i];
	  				
	  	// 		}
	  	// 		self.countExtraits.push({"id":child.key,"count": count});
	  	// 		//console.log(child.key," : ",count);
	  	// 		self.countExtraits = self.countExtraits.sort(function(a, b){return parseInt(b.count)-parseInt(a.count)});
	  	// 	});
	  	// 	//console.log("LES EXTRAITS : ", self.countExtraits);
	  	// });
	    let refEnjeu = self.db.list('enjeu').snapshotChanges().subscribe(function(data){
             		refEnjeu.unsubscribe();

                    let enjeux = data.map(a => ({ key: a.key, ...a.payload.val() }));

                    self.allEnjeux = enjeux.map((a:Enjeu) => new Enjeu().deserialize(a));
                    // console.log("self.allEnjeux %o",self.allEnjeux)
        });



	    // self.db.database.ref("clients/"+self.user.num).on('value', function(data){
     //        //for(let i in data.val().motcles){
     //        	self.mot_cle.push(data.val().motcles);
     //        //}
     //        console.log("Mot cles client %o", self.mot_cle);
     //  	})



	  	let refEnjeu2 = self.db.list("enjeu").snapshotChanges().subscribe(function(data){
	  		refEnjeu2.unsubscribe();
	  		let enjeux = data.map(a => ({ key: a.key, selected:false, ...a.payload.val() }));
	                self.enjeux = enjeux.map(function(a:any){
	                	a.id = "enjeu"+a.num.replace(/#/g, "");
	                	return new Enjeu().deserialize(a)
	        });

		    self.db.list("enjeux_extraits").snapshotChanges().subscribe(function(data){
		  		// for(let i in data.val()){
		  		// 	console.log(i);
		  		// 	console.log(data.val()[i]); 
		  			
		  		// }
		  		data.forEach(function(child){
		  			let count = 0;
		  			//console.log("ID",child.key);
		  			for(let i in child.payload.val()){
		  				count = count + child.payload.val()[i];
		  				
		  			}
		  			self.countExtraits.push({"id":child.key,"count": count});
		  			//console.log(child.key," : ",count);
		  			self.countExtraits = self.countExtraits.sort(function(a, b){return parseInt(b.count)-parseInt(a.count)});
		  		});
		  		//console.log("LES EXTRAITS : ", self.countExtraits);
	     		//self.enjeux = self.enjeux.filter((elem) => self.countExtraits.find((id) => elem.key === id.id));
	     		for(let enj of self.countExtraits){
	     			let list = self.enjeux.filter(x => x.key == enj.id);
	     			let removeIndex = self.enjeux.findIndex(itm => itm.key === enj.id);
	     			if(removeIndex !== -1){
	     				self.enjeux.splice(removeIndex,1);
	     			}
				      	list.forEach(x => {
					        self.enjeux.push(x);
				    	})
			    //     self.enjeux = self.enjeux.find((id) => enj.key === id.id));
			    }
	    
			    //console.log(self.filteredArray);
	     		// self.enjeux = self.enjeux.forEach(function(val){
	     		// 	console.log(val);
	     		// });
	     		//console.log("ENJEUX", self.enjeux);
		  	});

		  	self.db.list("clients/"+self.user.num+"/enjeu").snapshotChanges().subscribe(function(data){

		  		//console.log("ENJEUX", self.enjeux);
	            let client_enjeu = data.map(a => ({ cle: a.key, selected:false, ...a.payload.val() }));
	            self.client_enjeu = client_enjeu.map(function(a:any){

	            	a.id = "enjeu"+a.num.replace(/#/g, "");

	            	return self.enjeux.find((element) => element.num == a.num);
	            });
	            
	            self.countEnjeu = self.client_enjeu.length;
	            if(self.countEnjeu == 16){
	            	self.closeEnjeux();
	            }

	            for(let cli of self.enjeux){
	     			let list = self.client_enjeu.filter(x => x.key == cli.key);
	     			let removeIndex = self.client_enjeu.findIndex(itm => itm.key === cli.key);
	     			if(removeIndex !== -1){
	     				self.client_enjeu.splice(removeIndex,1);
	     			}
			      	list.forEach(x => {
				        self.client_enjeu.push(x);
			    	})
			    }

			    //console.log("self.client_enjeu %o",self.client_enjeu )

		    });

	    });

	  	self.db.database.ref("enjeux_extraits").once("value",function(extraitsData){
	  		self.extraits = extraitsData.val();
	  	})


	    let refPhrases = this.db.list("phrases").snapshotChanges().subscribe(function(data){
	    		refPhrases.unsubscribe();

	    		self.flux = [];
	    		self.dates = [];
	    		self.pays = [];
	    		self.years = [];
	    		let prev = [];
	    		self.csvPhrases = data.map( (a:any) => {

	                	return new CsvPhrase().deserialize(a.payload.val());
	            });

	            self.user.flux = self.user.flux.filter((v,i) => self.user.flux.indexOf(v) === i);
	    		self.flux = self.user.flux;


	    		self.csvPhrases = self.csvPhrases.filter(function(item){
	    			
	    				return self.checkFluxUser(item.flux);
	    			

	    		});

	    		self.csvPhrases = self.csvPhrases.map( (a:CsvPhrase) => {
	                		self.dates.push(a.date);
	                		self.pays.push(a.pays);

	                		if(a.date.split('/').length == 3){
	                			if(parseInt(a.date.split('/')[2]) >= 2000)
	                			{
	                				self.years.push(a.date.split('/')[2]);
	                			}
	                		}

	                	return a;
	            });

	    		self.csvPhrasesFiltered = self.csvPhrases;

	    		// console.log(self.csvPhrases);
	    		let refSeuil = self.db.object("seuil").snapshotChanges().subscribe(function(seuil){
       				refSeuil.unsubscribe();
		                if(seuil.payload.val()){
		                        self.seuil = parseInt(seuil.payload.val().toString());
		                }
		                let prom = new Promise((resolve, reject) => {
			    			self.csvPhrases.forEach((csv, index, array) =>{
				    			let refPhrase =self.db.object("phrases_csv/"+csv.nom).snapshotChanges().subscribe(function(phrasesSnap){

				    				refPhrase.unsubscribe();
				    				let phrases:Phrase[] = [];


									phrasesSnap.payload.forEach(function(child){
										// phrases.push(new Phrase().deserialize(child.val()));
										let phrase = new Phrase().deserialize(child.val());
										phrase["phrase"] = phrase["phrase_"+phrase.langue.toLowerCase()];
				    					let perf = self.getMaxPerfPhrase(phrase).perf;
				    					if(perf >= self.seuil){
				    						phrases.push(phrase);
				    					}
										return false;
									});
									self.phrases[csv.nom] = phrases;
									csv.phrases = phrases;


									if(index === array.length -1) resolve();
				    			});

				    		})
			    		});

			    		prom.then(() =>{

			    			jQuery(".main-container").css("margin-left", "342px");
			    			self.db.database.ref("clients/"+self.user.num+"/client_enjeu").once("value",function(data){
			       				let enjeu:Enjeu = new Enjeu();
			                    if(data.val()){
			                    	let enjeux = data.val();
			                    	self.selectedEnjeux = [];
			                    	for(let enj of enjeux){
			                    		self.selectedEnjeux.push(new Enjeu().deserialize(enj));
			                    	}
			                    	if(self.client_enjeu.length == 0){
			                    		self.selectedEnjeux = [];
			                    	}
			                    	// console.log("self.selectedEnjeux %o", self.selectedEnjeux);


			                    }else{
			                    	self.selectedEnjeux = [];
			                    	if(self.client_enjeu.length > 0){
			                    		self.selectedEnjeux.push(self.client_enjeu[0]);
			                    	}
			                    }

			                   	self.checkFilter();
			                    // self.chartOptions = self.getChartOptions2();

			                    self.loading = false;
			    				self.loadingMap = false;

			    				let refClientMots = self.db.list("clients/"+self.user.num+"/mots_cles").snapshotChanges().subscribe(function(data){

						     		self.mot_cle = data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
						     		if(self.mot_cle){
						     			self.checkFilter();

						     		}

						     	});


			         		});

			    		});

			   //  		jQuery(".main-container").css("margin-left", "342px");
			   //  		self.loading = false;
						// self.loadingMap = false;
			    		self.years =  self.years.filter((v,i) => self.years.indexOf(v) === i);
			    		self.pays =  self.pays.filter((v,i) => self.pays.indexOf(v) === i);
			    		self.years = self.years.sort(function(a, b){return parseInt(a)-parseInt(b)});

						self.checkTimelapse();

			    		// console.log("Flux %o", self.flux);

			    		// console.log("Pays %o", self.pays);










		        })








	    		// console.log("self.phrases : %o",self.phrases );

	    		////Charts








	    });

	}

	checkYear()
	{
		//this.options.barDimension = 100;
		let years = this.years.map((function(item) {
				return parseInt(item, 10);
			}));
		//if(this.dateFilterToggle == false || this.dateFilterToggle == undefined)
		if(this.dateFilterToggle == true)
		{

			let sliderArray = [];
			for(let i=0; i<this.years.length; i++)
			{
				sliderArray.push({value: this.years[i]})
			}

			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value:number) =>{
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value:number) =>{
				return "#004FA3";
			})
			// this.options.showTicks = false;
			this.options.stepsArray = sliderArray;

			this.value = Math.min( ...years );


			this.yearFilter = this.value.toString();
			this.checkFilter();
			this.dateFilterToggle = false;
		}

	}

	checkTimelapse()
	{
		//this.options.barDimension = 100;
		this.yearFilter = false;
		let years = this.years.map((function(item) {
				return parseInt(item, 10);
			}));
		if(this.dateFilterToggle == false || this.dateFilterToggle == undefined)
		//if(this.dateFilterToggle == true)
		{
			let sliderArray = [];
			for(let i=0; i<this.years.length; i++)
			{
				sliderArray.push({value: this.years[i], legend: ''})
			}
			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value:number) =>{
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value:number) =>{
				return "#004FA3";
			})
			this.options.stepsArray = sliderArray;
			this.options.floor = Math.min( ...years );
			this.options.ceil = Math.max( ...years );
			this.value = Math.min( ...years );
			this.highValue = Math.max( ...years );


			this.year_from = this.value.toString();
			this.year_to = this.highValue.toString();
			this.test2 = false;
			this.checkFilter();
			this.dateFilterToggle = true;
		}
	}

	dateRangeChange(changeContext: ChangeContext)
	{
		this.year_from = changeContext.value.toString();
		this.year_to = changeContext.highValue.toString();
		this.loadingMap = true;
		this.checkFilter();
	}

	dateSlideChange(changeContext: ChangeContext)
	{
		this.year_from = null;
		this.year_to = null;
		this.yearFilter = changeContext.value.toString();
		this.loadingMap = true;
		this.checkFilter();
	}

	ngAfterViewInit(){

			// sidebar
		  	this.isActive = false;
		  	this.isActive2 = true;
		  	this.isActive3 = false;
		  	this.collapsed2 = true ;
		  	this.collapsed3 = true ;
	        this.collapsed = false;
	        this.showMenu = '';
	        this.pushRightClass = 'push-right';
			this.pushLeftClass = 'push-right';
			//this.defaultEnjeu();
	}

	onFluxFilterChange(e:any){


		this.allFluxFilter = false;
		this.loadingMap = true;
		this.csvPhrasesFiltered = [];
		let self = this;
		//console.log(this.fluxFilter);
		this.test2 = false;
		if(this.fluxFilter.length === 0)
		{
			//console.log("vide checked ");
			this.test2 = true;
		}
		if(this.fluxFilter.some(e => e === true))
		{
			//console.log("array checked");
			this.test2 = true;
		}
		if(e === true)
		{
			//console.log("one checked");
			this.test2 = true;
		}
		this.test = true;
		
		//this.test2 = true;
		this.checkFilter();
		//this.checkMinMaxYear(this.csvPhrasesFiltered);
	}

	onNewPaysFilterChange(e:any,p){
		let self = this;
		self.loadingMap = true;
		self.csvPhrasesFiltered = [];
		let test = true;
		// let res = false;
		// for(let i in this.newpaysFilter){
		// 	if(this.newpaysFilter[i]){
		// 		res = true;
		// 		this.allPaysFilter = false;
		// 		break;
		// 	}
		// }
		if(e){
			self.allPaysFilter = false;
			//this.all_pays = "";
			this.all_pays = "";
			this.liste_pays.push(p);
		} else {
			for(let i=0;i<this.liste_pays.length;i++){
				if (this.liste_pays[i] === p) {
			        this.liste_pays.splice(i, 1);
			    }
			}
		}
		self.checkFilter();

	}

	onPerfFilterChange(e:any){
		this.loadingMap = true;
		this.csvPhrasesFiltered = [];
		let self = this;
		self.allPerf = false;
		this.checkFilter();


	}

	onAllPaysFilterChange(e:any){
		this.loadingMap = true;
		if(this.allPaysFilter){
			this.all_pays = "Indifférent";
			this.liste_pays = [];
			for(let i in this.newpaysFilter){
			 	this.newpaysFilter[i] = false;
			}
			this.allPaysFilter = false;
		} else {
			this.all_pays = "";
		}

		this.checkFilter("allPays");
	}

	onFluxAllFilterChange(e:any){
		let self = this;
		this.loadingMap = true;
		if(this.allFluxFilter){

			for(let i in this.fluxFilter){
				this.fluxFilter[i] = false;
			}
			this.allFluxFilter = false;
			self.years = [];
			self.csvPhrases.map( (a:CsvPhrase) => {
          		if(a.date.split('/').length == 3){
          			if(parseInt(a.date.split('/')[2]) >= 2000)
          			{
          				self.years.push(a.date.split('/')[2]);
          			}
          		}

          	return a;
      		});
      		this.years =  this.years.filter((v,i) => this.years.indexOf(v) === i);
			this.years = this.years.sort(function(a, b){return parseInt(a)-parseInt(b)});
			this.filter = false;
			this.checkFilter();
		}


		// this.checkTimelapse();
		this.dateFilterToggle = false;
		this.checkTimelapse();




		this.showSlider = false;
		setTimeout(()=>
		{
      		this.showSlider = true;
 		}, 50);

	}

	resetFluxFilter(e:any){
		
		let self = this;
		//this.loadingMap = true;
		if(this.allFluxFilter){
			this.allFluxFilter = false;
			self.years = [];
			self.csvPhrases.map( (a:CsvPhrase) => {
          		if(a.date.split('/').length == 3){
          			if(parseInt(a.date.split('/')[2]) >= 2000)
          			{
          				self.years.push(a.date.split('/')[2]);
          			}
          		}

          	return a;
      		});
      		//this.years =  this.years.filter((v,i) => this.years.indexOf(v) === i);
      		this.years =  this.years.filter((v,i) => this.years.indexOf(v) === i && v !== undefined && parseInt(v) > 2000);
			this.years = this.years.sort(function(a, b){return parseInt(a)-parseInt(b)});
			//this.checkFilter("allFlux");
		}
		this.test2 = false
		this.yearFilter = false;
		this.year_from = undefined;
		this.year_to= undefined;

		this.showSlider = false;
		setTimeout(()=>
		{
      		this.showSlider = true;
 		}, 50);
	}

	onAllPerfFilterChange(e:any){
		let self = this;
		this.loadingMap = true;

		//if(self.allPerf){
			for(let i in self.perfFilter){
				self.perfFilter[i] = false;
			}
			self.allPerf = false;
		//}



		this.checkFilter("allPerf");
	}

	onYearFilterChange(e:any){
		//this.options.barDimension = 100;
		this.showFilterDate = this.yearFilter == "definie" ? true : false;
		this.showSliderDate = this.yearFilter == "slider" ? true : false;
		this.showRangeDate = this.yearFilter == "range" ? true : false;
		this.loadingMap = true;
		if(this.yearFilter == 'slider')
		{
			let sliderArray = [];
			for(let i=0; i<this.years.length; i++)
			{
				sliderArray.push({value: this.years[i], legend: ''})
			}
			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value:number) =>{
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value:number) =>{
				return "#004FA3";
			})
			this.options.stepsArray = sliderArray;
			this.value = 2014;
		}
		if(this.yearFilter == 'range')
		{
			let sliderArray = [];
			for(let i=0; i<this.years.length; i++)
			{
				sliderArray.push({value: this.years[i], legend: ''})
			}
			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value:number) =>{
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value:number) =>{
				return "#004FA3";
			})
			this.options.stepsArray = sliderArray;
			this.value = 2014;
			this.highValue = 2019;
		}

		//this.checkFilter();

	}

	onDateFilterChange(e:any, type:string){
		switch (type) {
			case "month_from":
				if(this.year_from){
					this.loadingMap = true;
					this.checkFilter();
				}
				break;
			case "year_from":
				if(this.month_from){
					this.loadingMap = true;
					this.checkFilter();
				}
				break;
			case "month_to":
				if(this.year_to){
					this.loadingMap = true;
					this.checkFilter();
				}
				break;

			case "year_to":
				if(this.month_to){
					this.loadingMap = true;
					this.checkFilter();
				}
				break;

			default:
				// code...
				break;
		}
	}

	onPaysFilterChange(e){

		this.loadingMap = true;
		this.checkFilter();

	}

	getChartOptions2(){

	    this.fluxData = [];
	    let csvData = [];
	    let self = this;

	    //get flux
	    // console.log("this.fluxFilter %o", this.fluxFilter);
	    if(self.selectedEnjeux.length == 0){
	    	if(self.client_enjeu && self.client_enjeu.length > 0){
	    		self.selectedEnjeux = [];
	        	self.selectedEnjeux.push(self.client_enjeu[0]);
	        }
	    }


	    for(let i in this.flux){
	    	let ok = false;
			if(this.fluxFilter[i] && this.fluxFilter[i] != undefined){
	    		ok = true;
			}
			if(!this.checkFluxFilter()){
				ok = true;
			}


			// console.log("checkFluxFilter %o", !this.checkFluxFilter());

	    	if(ok){
	    		// console.log("this.flux[%o] %o",i, this.flux[i]);
	    		let value = 0;
	    		// console.log("csvPhrasesFiltered %o", this.csvPhrasesFiltered);


	    		let csvPhrases = this.csvPhrasesFiltered.filter(function(a){
						let res = false;
							// console.log("extraits %o",a.extraits);

							if(self.cleanString(a.flux) == self.cleanString(self.flux[i].flux)){
								res = true;
							}


						return res;
				});

	    		let promises = [];




		    		// console.log("self.fluxData %o",self.fluxData);



	    		// console.log("this.fluxData %o", this.fluxData);
	    		let totalFlux = 0;
	    		for(let c in csvPhrases){

	    			if(self.cleanString(csvPhrases[c].flux) == self.cleanString(this.flux[i].flux)){
	    				let total = 0;

						let iter = 0;
						let satVal = 0;
						let satKey = "1";
						let enjeuDefined = false;

						if(self.selectedEnjeux && self.selectedEnjeux.length > 0){
							// let enj = self.selectedEnjeux[0].num.replace(/#/g, "");
							// if((csvPhrases[c].sat && csvPhrases[c].sat[enj] == undefined) || csvPhrases[c].sat == undefined ){
							// 	enjeuDefined = false;
							// }else{
							// 	for(let s in csvPhrases[c].sat[enj]){
							// 		if(parseInt(csvPhrases[c].sat[enj][s]) > satVal){
							// 				satVal = parseInt(csvPhrases[c].sat[enj][s]);
							// 				satKey = s;
							// 		}
							// 	}
							// }
							if(csvPhrases[c].phrases){
								satKey = self.computeCsvSat(csvPhrases[c].phrases, self.selectedEnjeux[0]);
								enjeuDefined = true;
								// console.log("satKey %o csv %o", satKey, csvPhrases[c].nom);
							}


						}




						let sat = satKey;


						let extraitPhrases = 0;

						let itemStyle:any = {color: this.couleurPhrase[sat]};
						if(csvPhrases[c].nom == self.csvSelected){
							itemStyle = {color: this.couleurPhrase[sat], borderWidth: 1, borderColor: '#004085'};
						}

						if((csvPhrases[c].phrases == undefined) || (csvPhrases[c].phrases && csvPhrases[c].phrases.length == 0)){
							itemStyle.color = self.getColor("Objectif non traité");
						}



						if(csvData.find((csv) => csv.nomCsv == csvPhrases[c].nom)){
							continue;
						}
							csvData.push({
									value: total,
									name: "extraits",
									itemStyle: itemStyle,
									nomCsv: csvPhrases[c].nom,
									titre : self.translate.currentLang == "en" ? csvPhrases[c].titre_en.substring(0, 20) : csvPhrases[c].titre_fr.substring(0, 20)

							});



	    			}

	    		}

	    		self.fluxData.push({
		    					value: csvPhrases.length,
		    					name: self.flux[i].flux,
		    					itemStyle: {color: self.getColor(self.flux[i].flux)},
		    					documents: csvPhrases.length
		    	});


	    	}



	    }

	    let options = {

					    tooltip: {
					        trigger: 'item',
					        formatter: function(params){
					        	// console.log("params: %o",params);
					        	let data = "";
					        	if(params.seriesIndex == 0){
					        		data = params.marker+""+params.name.toUpperCase()+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;"+params.data.documents+" documents";
					        	} else {
					        		data = params.marker+" "+params.data.titre;
					        	}

					        	return data;
					        }

					    },
					    // legend: {
					    //     orient: 'vertical',
					    //     x: 'left',
					    //     data:['直达','营销广告','搜索引擎','邮件营销','联盟广告','视频广告','百度','谷歌','必应','其他']
					    // },
					    series: [
					        {
					            /*name:this.fluxDataName,*/
					            name:'',
					            type:'pie',
					            selectedMode: 'single',

					            radius: [0, '30%'],

					            label: {
					                normal: {
					                    position: 'inner',
					                    show: false,
					                    // formatter: "{a}<i class='fa fa-circle'> {b}:<br/> {c} extraits ({d}%)"
					                }
					            },
					            labelLine: {
					                normal: {
					                    show: false
					                }
					            },
					            data: this.fluxData
					        },
					        {
					            /*name:'Document',*/
					            name:'',
					            type:'pie',
					            radius: ['40%', '55%'],
					            label: {
					                normal: {
					                    // formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c} ',
					                    backgroundColor: '#eee',
					                    borderColor: '#aaa',
					                    borderWidth: 1,
					                    borderRadius: 4,
					                    show : false,
					                    // shadowBlur:3,
					                    // shadowOffsetX: 2,
					                    // shadowOffsetY: 2,
					                    // shadowColor: '#999',
					                    // padding: [0, 7],
					                    rich: {
					                        a: {
					                            color: '#999',
					                            lineHeight: 22,
					                            align: 'center'
					                        },
					                        // abg: {
					                        //     backgroundColor: '#333',
					                        //     width: '100%',
					                        //     align: 'right',
					                        //     height: 22,
					                        //     borderRadius: [4, 4, 0, 0]
					                        // },
					                        hr: {
					                            borderColor: '#aaa',
					                            width: '100%',
					                            borderWidth: 0.5,
					                            height: 0
					                        },
					                        b: {
					                            fontSize: 16,
					                            lineHeight: 33
					                        },
					                        per: {
					                            color: '#eee',
					                            backgroundColor: '#334455',
					                            padding: [2, 4],
					                            borderRadius: 2
					                        }
					                    }
					                }
					            },
					            data: csvData
					        }
					    ]
					};
		return options;
	}



	onChartEvent(e:any, type:string){
		this.csvClick = true;
		this.sideEnjeux = false;
		let index = 0;
		if(e.seriesIndex == 1){
			this.csvSelected = e.data.nomCsv;


			// this.checkFilter();
			for(let i in this.csvPhrasesFiltered){
				if(e.data.nomCsv == this.csvPhrasesFiltered[i].nom){
					this.csvPhrase = this.csvPhrasesFiltered[i];
					break;
				}
				index+=1;
			}


			let self = this;

			// if(self.phrases[this.csvPhrase.nom] == undefined){
			// 	// console.log("here %o", this.csvPhrase.nom);

			// 	let ref = self.db.object("phrases_csv/"+this.csvPhrase.nom).snapshotChanges().subscribe(function(phrasesSnap){

			// 		// ref.unsubscribe();
			// 		let phrases:Phrase[] = [];
			// 		phrasesSnap.payload.forEach(function(child){
			// 			phrases.push(new Phrase().deserialize(child.val()));
			// 			return false;
			// 		});

			// 		self.csvPhrase.phrases = phrases.filter(function(value:Phrase, index:number, array:Phrase[]){
			// 	            let perf = self.getMaxPerfPhrase(value).perf;
			// 	            value.performance = perf.toString();
			// 	            let ok = false;
			// 	            if(self.selectedEnjeux.length > 0){
			// 	            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);
			// 					if(check != undefined){
			// 						ok = true;
			// 					}
			// 	            }else{
			// 	            	ok = true;
			// 	            }


			// 	            return (perf >= self.seuil) && ok;

		 //          		});


			// 	});
			// }else{
			// 	self.csvPhrase.phrases = self.phrases[this.csvPhrase.nom].filter(function(value:Phrase, index:number, array:Phrase[]){
		 //            let perf = self.getMaxPerfPhrase(value).perf;
		 //            let ok = false;
		 //            value.performance = perf.toString();
		 //            console.log("self.selectedEnjeux %o", self.selectedEnjeux);
		 //            if(self.selectedEnjeux.length > 0){
		 //            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);

			// 			if(check != undefined){
			// 				ok = true;
			// 			}
		 //            }else{
		 //            	ok = true;
		 //            }


		 //            return (perf >= self.seuil) && ok;

		 //          });

			// 	console.log("self.csvPhrase.phrases %o", self.csvPhrase.phrases);
			// }
				self.collapsed2 = false;
		        self.collapsedEvent.emit(self.collapsed2);


		}

		this.chartOptions =  this.getChartOptions2();
	}



	getCsvData(){
		let res = [];
		let self = this;
		for(let i in this.csvPhrasesFiltered){
						let satVal = 0;
						let satKey = "1";
						let enjeuDefined = true;
						let csvPhrase = this.csvPhrasesFiltered[i];
						if(self.selectedEnjeux && self.selectedEnjeux.length > 0){
							let enj = self.selectedEnjeux[0].num.replace(/#/g, "");
							if(csvPhrase.sat[enj] == undefined){
								enjeuDefined = false;
							}else{
								for(let s in csvPhrase.sat[enj]){
									if(parseInt(csvPhrase.sat[enj][s]) > satVal){
											satVal = parseInt(csvPhrase.sat[enj][s]);
											satKey = s;
									}
								}
							}

						}
						let sat = satKey;
						let color = this.couleurPhrase[sat];

						if(!enjeuDefined){
							color = "#F44336";
						}





			res.push({
				value:this.csvPhrasesFiltered[i].extraits,
				// name: ""+this.csvPhrasesFiltered[i].phrases.length.toString()+" extraits",
				name: "Extraits",
				itemStyle: {color: color}

			});
		}
		// console.log(this.csvPhrasesFiltered);
		return res;
	}

	checkFilter(filter=""){
		let self = this;
		self.loadingMap = true;
		setTimeout(function(){

		// console.log("checking filter ");

		self.csvPhrasesFiltered = [];
		for(let i in self.csvPhrases){
			self.csvPhrasesFiltered.push(self.csvPhrases[i]);

		}



					if(self.enjeuOver){

							// let csvName = {};

							// for(let enj of self.selectedEnjeux){

							// 		for(let csv in self.enjeuxExtraits[enj.num.replace(/#/g, "")]){
							// 			csvName[csv] = true;
							// 		}
							// }
							// if(self.selectedEnjeux.length > 0){
							// 	self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
							// 			let res = false;

							// 			if(csvName[a.nom] != undefined){
							// 					res = true;
							// 			}

							// 			return res;
							// 	});
							// }else{
							// 	self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
							// 			let res = false;

							// 			if(self.enjeuxExtraits[self.enjeuOver.num.replace(/#/g, "")] && self.enjeuxExtraits[self.enjeuOver.num.replace(/#/g, "")][a.nom] != undefined){
							// 					res = true;
							// 			}

							// 			return res;
							// 	});
							// }



							if(self.csvPhrase.phrases){
								// let ref = self.db.object("phrases_csv/"+self.csvPhrase.nom).snapshotChanges().subscribe(function(phrasesSnap){
								// 	ref.unsubscribe();
								// 	let phrases:Phrase[] = [];
								// 	phrasesSnap.payload.forEach(function(child){
								// 		phrases.push(new Phrase().deserialize(child.val()));
								// 		return false;
								// 	});
									self.csvPhrase.phrases = self.phrases[self.csvPhrase.nom] ? self.phrases[self.csvPhrase.nom].filter(function(value:Phrase, index:number, array:Phrase[]){

												let ok = false;

										        if(self.enjeuOver && (value.enjeu == self.enjeuOver.num)){
										            ok = true;
										        }
									            return ok;

			         				}) : [];



								// });


							}

						}else{

							if(self.selectedEnjeux.length > 0){

								self.csvPhrasesFiltered.map((csvPhrase, index, array) =>{
										csvPhrase.phrases = self.phrases[csvPhrase.nom] ? self.phrases[csvPhrase.nom].filter((phrase:Phrase) =>{
										let res = false;
										if(phrase.enjeu == self.selectedEnjeux[0].num){
											res = true;
										}
										return res;
									}) : [];
									return csvPhrase;
								});


							}else{
								self.csvPhrasesFiltered.map((csvPhrase, index, array) =>{
									csvPhrase.phrases = [];
									return csvPhrase;
								})
							}
						}



					if(self.checkFluxFilter()){

							self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
								let res = false;
								for(let i in self.fluxFilter){
									if(self.fluxFilter[i]){
											if(self.cleanString(a.flux) == self.cleanString(self.flux[i].flux)){

												res = true;
												return res;
											}

									}
								}

								return res;
							});


						}
						else
						{	
							if(this.test2)
							{
								console.log("test2 : ",this.test2)
								this.resetFluxFilter("");
							}
							
						}
						if(!this.allFluxFilter && this.test)
						{
							self.checkMinMaxYear(self.csvPhrasesFiltered);
						}

						if(self.checkPaysFilter()){
							self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
								let res = false;
								for(let i in self.newpaysFilter){
									if(self.newpaysFilter[i]){
										//for(let j in a.phrases){
											//console.log(a.phrases[j].pays);
											// console.log(a.pays);
											if(a.pays.toLowerCase() == self.pays[i].toLowerCase()){
												res = true;
												return res;
											}
										//}
									}
								}

								return res;

							})
						}

						if(self.checkPerfFilter() ){
							self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
									let res = false;
									if(a.sat){
										for(let i in self.perfFilter){
											if(self.perfFilter[i]){

													if( self.selectedEnjeux.length > 0 && a.sat[self.selectedEnjeux[0].num.replace(/#/g, "")] && a.sat[self.selectedEnjeux[0].num.replace(/#/g, "")][i.toString()]){
														res = true;
														break;
													}




											}
										}
									}

									return res;
							})
						}







						if(self.yearFilter){
							self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
									let res = false;

									if(a.date.split('/').length == 3){
										if(a.date.split('/')[2] == self.yearFilter){
											//console.log("eto isika yearfilter : ",self.yearFilter);
											res = true;
										}
									}
									return res;
							})
						}

						/*if(self.month_from && self.year_from){
							self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
								let res = false;
								if(a.date.split('/').length == 3){
									let date  = Date.parse(a.date.split('/')[2]+"-"+a.date.split('/')[1]+"-"+a.date.split('/')[0]);
									let date_from = Date.parse(self.year_from+"-"+self.month_from);
									if(date >= date_from){
										res = true;
									}
								}
								return res;
							})

						}

						if(self.month_to && self.year_to){
							self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
								let res = false;
								if(a.date.split('/').length == 3){
									let date  = Date.parse(a.date.split('/')[2]+"-"+a.date.split('/')[1]+"-"+a.date.split('/')[0]);
									let date_to = Date.parse(self.year_to+"-"+self.month_to);

									if(date <= date_to){
										res = true;
									}
								}
								return res;
							})

						}
*/

						if(self.year_from){
							self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
								let res = false;
								if(a.date.split('/').length == 3){
									//console.log("year from : ",self.year_from)
									let date  = Date.parse(a.date.split('/')[2]);//+"-"+a.date.split('/')[1]+"-"+a.date.split('/')[0]);
									let date_from = Date.parse(self.year_from);
									if(date >= date_from){
										res = true;
									}
								}
								return res;
							})
						}

						if(self.year_to){
							self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
								let res = false;
								if(a.date.split('/').length == 3){
									let date  = Date.parse(a.date.split('/')[2])//+"-"+a.date.split('/')[1]+"-"+a.date.split('/')[0]);
									let date_to = Date.parse(self.year_to);

									if(date <= date_to){
										res = true;
									}
								}
								return res;
							})
						}

						if(self.paysFilter != "all"){
							self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
									let res = false;


										if(a.pays == self.paysFilter){
											res = true;
										}

									return res;
							})
						}



						// console.log("MOT CLE %o", self.mot_cle);

						if(self.mot_cle && self.mot_cle.length > 0){

								// console.log("MOT CLE INSIDE %o", self.mot_cle);

									self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
										let res = false;
										let test = false;
											// titre fr

											for(let i in self.mot_cle){

												let motEntree = self.mot_cle[i].mot;
												console.log("MOT ENTREE %o", motEntree);
												if(a.nom.toLowerCase() == motEntree.toLowerCase()){
													res=true;
													test=true;
												}

													let mot_test = a.titre_fr.toLowerCase();
													if(mot_test.includes(motEntree.toLowerCase())){
														res = true;
														test = true;
													}


												//titre en

												if(!test){
													let mot_test = a.titre_en.toLowerCase();
													if(mot_test.includes(motEntree.toLowerCase())){
														res = true;
														test = true;
													}
												}


												//phrase fr
												if(!test && a.phrases){
													for(let i in a.phrases){

														let mot_test = a.phrases[i].phrase_fr.toLowerCase();
														if(mot_test.includes(motEntree.toLowerCase())){
															res = true;
															test = true;
															break;
														}
													}
												}

												//phrase en
												if(!test && a.phrases){
													for(let i in a.phrases){

														let mot_test = a.phrases[i].phrase_en.toLowerCase();
														if(mot_test.includes(motEntree.toLowerCase())){
															res = true;
															test = true;
															break;
														}
													}
												}
											}







										return res;
									});
						}







			self.loadingMap = false;

			this.chartOptions = this.getChartOptions2();


		  }.bind(this),250);
	}


	checkFluxFilter(){
		let res = false;
		for(let i in this.fluxFilter){

			if(this.fluxFilter[i]){
				res = true;
				break;
			}
		}
		if(!res){
			this.allFluxFilter = true;			
			//this.resetFluxFilter("");
		}
		return res;
	}

	checkPaysFilter(){
		let res = false;
		for(let i in this.newpaysFilter){
			if(this.newpaysFilter[i]){
				this.all_pays = "";
				res = true;
				break;
			} else {

				//this.liste_pays = [];
			}
		}
		if(!res){
			this.all_pays = "Indifférent";
			this.liste_pays = [];
			this.allPaysFilter = true;
		}

		return res;
	}

	checkMot(mot){
		let res = true;

		//test fr
		for(let i in this.titre_fr){
			let titre = this.titre_fr[i].toLowerCase();
			let test = titre.includes(mot.toLowerCase());
			if(res){
				if(test){
					//this.mot_cle.push(mot);
					res = false;
				}
			}
		}

		// console.log(res);

		//test en
		for(let i in this.titre_en){
			let titre = this.titre_en[i].toLowerCase();
			let test = titre.includes(mot.toLowerCase());
			if(res){
				if(test){
					//this.mot_cle.push(mot);
					res = false;
				}
			}
		}

		//phrase fr
		for(let i in this.phrase_fr){
			let titre = this.phrase_fr[i].toLowerCase();
			let test = titre.includes(mot.toLowerCase());
			if(res){
				if(test){
					//this.mot_cle.push(mot);
					res = false;
				}
			}
		}

		//phrase en
		for(let i in this.phrase_en){
			let titre = this.phrase_en[i].toLowerCase();
			let test = titre.includes(mot.toLowerCase());
			if(res){
				if(test){
					//this.mot_cle.push(mot);
					res = false;
				}
			}
		}

		return res;
	}



	checkPerfFilter(){
		let res = false;
		for(let i in this.perfFilter){
			if(this.perfFilter[i]){
				res = true;
				break;
			} else if((!this.perfFilter[1] && !this.perfFilter[2]) && (!this.perfFilter[3] && !this.perfFilter[4])){
				this.allPerf = true;
			}
		}
		return res;
	}

	checkMinMaxYear(csvPhrases) {
		//this.options.barDimension = 100;
		this.years = [];
		csvPhrases.map(phrase =>{
			this.years.push(phrase.date.split('/')[2])
		})
		this.years =  this.years.filter((v,i) => this.years.indexOf(v) === i && v !== undefined && parseInt(v) > 2000);
		this.years = this.years.sort(function(a, b){return parseInt(a)-parseInt(b)});

		//this.checkTimelapse();


		let years = this.years.map((function(item) {
				return parseInt(item, 10);
			}));


		//////// timelapse ///////
		if(this.dateFilterToggle === true)
		{
			this.yearFilter = false;
			let sliderArray = [];
			for(let i=0; i<years.length; i++)
			{
				sliderArray.push({value: years[i], legend: ''})
			}
			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value:number) =>{
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value:number) =>{
				return "#004FA3";
			})
			this.options.stepsArray = sliderArray;
			this.value = Math.min( ...years );
			this.highValue = Math.max( ...years );
			this.year_from = this.value.toString();
			this.year_to = this.highValue.toString();
			this.dateFilterToggle = false;
			setTimeout(()=>
			{
      this.dateFilterToggle = true;
 			}, 50);
		}
		else
		{
			let sliderArray = [];
			for(let i=0; i<years.length; i++)
			{
				sliderArray.push({value: years[i]})
			}

			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value:number) =>{
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value:number) =>{
				return "#004FA3";
			})
			// this.options.showTicks = false;
			this.options.stepsArray = sliderArray;

			this.value = Math.min( ...years );


			this.yearFilter = this.value.toString();
			this.dateFilterToggle = true;
			setTimeout(()=>
			{
      this.dateFilterToggle = false;
 			}, 50);
		}
		this.test = false;
	}
	checkEnjeuFilter(){
		let res = false;
		for(let i in this.enjeux){
			if(this.enjeux[i].selected){
				res = true;
				break;
			}
		}
		return res;
	}

	checkFluxUser(flux:string, cons = false){
		let res = false;
		for(let i in this.user.flux){
			if(cons){

				// console.log("'%o' == '%o' : %o",this.user.flux[i].flux.toLowerCase().trim(),flux.toLowerCase().trim(),  this.cleanString(this.user.flux[i].flux) == this.cleanString(flux));

			}

			if(this.cleanString(this.user.flux[i].flux) == this.cleanString(flux)){
				res = true;
				break;
			}
		}
		return res;
	}

	cleanString(s){
	  return s.toLowerCase().trim().replace(/[^a-z0-9]/ig,'');
	}


	selectEnjeu(enjeu:Enjeu){


		this.selectedEnjeux = [];
		this.selectedEnjeux.push(enjeu);
		this.db.database.ref("clients/"+this.user.num+"/client_enjeu").set(this.selectedEnjeux);
		this.checkFilter();

		// if(this.enjeu){
		// 	if(this.enjeu.num == enjeu.num){
		// 		this.enjeu = undefined;
		// 	}else{
		// 		this.enjeu = enjeu;
		// 	}
		// }else{
		// 	this.enjeu = enjeu;
		// }

		// this.loadingMap = true;
		// this.checkFilter();

	}

	styleEnjeu(enjeu:Enjeu){
		if(this.selectedEnjeux){
			let check = this.selectedEnjeux.find((enj) => enj.num == enjeu.num);
			return (check != undefined) ? "select" : "primary";
		}
		return "primary";

	}

	overEnjeu(enjeu:Enjeu){
		this.enjeuOver = enjeu;

		this.loadingMap = true;
		this.checkFilter();
	}

	leaveEnjeu(enjeu:Enjeu){
		this.enjeuOver = undefined;

		this.loadingMap = true;
		this.checkFilter();
	}

	infoEnjeu(content, enjeu:Enjeu){
		this.enjeuInfo = enjeu;
		this.open(content);
	}

	sideEnjeu(){
		this.sideEnjeux = true;
		//this.collapsed2 = !this.collapsed2;
		this.collapsed3 = false;
	    this.collapsedEvent.emit(this.collapsed3);

	}

	closeEnjeux(){
		this.sideEnjeux = false;
		this.collapsed3 = !this.collapsed3;
	    this.collapsedEvent.emit(this.collapsed3);
	}
	closeEnjeux2(){
		this.toggleSidebar2();
	}

	addEnjeu(enjeu){
		let self = this;
		let data = [];
		if(self.selectedEnjeux.length == 0){
			self.selectedEnjeux.push(enjeu);
		}

		if(self.client_enjeu.length<16){
			self.db.database.ref("clients/"+self.user.num+"/enjeu/"+enjeu.key).set(enjeu);

			if(self.enjeu == undefined){
				self.enjeu = enjeu;
				self.loadingMap = true;
				self.checkFilter();
			}
		}
	}

	searchWord(mot){
		if(mot && mot != ""){
			this.db.database.ref("clients/"+this.user.num+"/mots_cles").push({mot:mot.toLowerCase()});
			this.loadingMap = true;
			this.checkFilter();
		}
		// console.log(mot.length);
		// if(mot != undefined && mot != null && mot.length > 0){
		// 	let res = true;

		// 	this.db.database.ref("clients/"+this.user.num+"/mots_cles").push({mot:mot.toLowerCase()});

		// 	//test fr
		// 	for(let i in this.titre_fr){
		// 		let titre = this.titre_fr[i].toLowerCase();
		// 		let test = titre.includes(mot.toLowerCase());
		// 		if(res){
		// 			if(test){
		// 				//this.mot_cle.push(mot);
		// 				res = false;
		// 			}
		// 		}
		// 	}

		// 	//test en
		// 	for(let i in this.titre_en){
		// 		let titre = this.titre_en[i].toLowerCase();
		// 		let test = titre.includes(mot.toLowerCase());
		// 		if(res){
		// 			if(test){
		// 				//this.mot_cle.push(mot);
		// 				res = false;
		// 			}
		// 		}
		// 	}

		// 	//phrase fr
		// 	for(let i in this.phrase_fr){
		// 		let titre = this.phrase_fr[i].toLowerCase();
		// 		let test = titre.includes(mot.toLowerCase());
		// 		if(res){
		// 			if(test){
		// 				//this.mot_cle.push(mot);
		// 				res = false;
		// 			}
		// 		}
		// 	}

		// 	//phrase en
		// 	for(let i in this.phrase_en){
		// 		let titre = this.phrase_en[i].toLowerCase();
		// 		let test = titre.includes(mot.toLowerCase());
		// 		if(res){
		// 			if(test){
		// 				//this.mot_cle.push(mot);
		// 				res = false;
		// 			}
		// 		}
		// 	}

		// 	//if(!res){
		// 		this.loadingMap = true;
		// 		this.checkFilter();
		// 	//}
		// } else {
		// 	this.loadingMap = true;
		// 	this.checkFilter();
		// }

	}

	deleteMotCles(motkey){
		// console.log(motkey);
		let self = this;
		self.db.database.ref("clients/"+self.user.num+"/mots_cles/"+motkey).remove();
		self.mot_cle = self.mot_cle.filter((value, index) =>{
			return index != motkey;
		});
		self.motEntree = undefined;
		this.loadingMap = true;
		this.checkFilter();
	}

	deleteEnjeuClient(key,enjeu){

		let self = this;

     	self.db.database.ref("clients/"+self.user.num+"/enjeu/"+enjeu.key).once("value",function(data){
 			let check = self.selectedEnjeux.find((enj) => enj.num == data.val().num);
 			console.log("HERE CHECK %o", check);
          	if(check){
          		self.selectedEnjeux = [];
          		self.db.database.ref("clients/"+self.user.num+"/client_enjeu").set(self.selectedEnjeux);

          	}
          	console.log("HERE self.selectedEnjeux  %o", self.selectedEnjeux);
          	self.db.database.ref("clients/"+self.user.num+"/enjeu/"+enjeu.key).remove();
          	self.checkFilter();
        });



	}

	selectCsvPhrase2(csv:CsvPhrase){
		this.csvClick = true;
		this.sideEnjeux = false;
		this.csvPhrase = csv;
		this.csvSelected = csv.nom;
		let self = this;

		// if(self.phrases[csv.nom] == undefined){
		// 	let ref = self.db.object("phrases_csv/"+csv.nom).snapshotChanges().subscribe(function(phrasesSnap){
		// 		ref.unsubscribe();
		// 		let phrases:Phrase[] = [];
		// 		phrasesSnap.payload.forEach(function(child){
		// 			phrases.push(new Phrase().deserialize(child.val()));
		// 			return false;
		// 		});

		// 		self.csvPhrase.phrases = phrases.filter(function(value:Phrase, index:number, array:Phrase[]){
	 //            let perf = self.getMaxPerfPhrase(value).perf;
	 //            let ok = false;
	 //            if(self.selectedEnjeux.length > 0){
	 //            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);
		// 			if(check != undefined){
		// 				ok = true;
		// 			}
	 //            }else{
	 //            	ok = true;
	 //            }


	 //            return (perf >= self.seuil) && ok;

	 //          })
		// 	});
		// }else{
		// 	self.csvPhrase.phrases = self.phrases[csv.nom].filter(function(value:Phrase, index:number, array:Phrase[]){
	 //            let perf = self.getMaxPerfPhrase(value).perf;
	 //            let ok = false;
	 //            if(self.selectedEnjeux.length > 0){
	 //            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);

		// 			if(check != undefined){
		// 				ok = true;
		// 			}
	 //            }else{
	 //            	ok = true;
	 //            }


	 //            return (perf >= self.seuil) && ok;

	 //          });
		// }
	        self.loading = false;



	}

	selectCsvPhrase(csv:CsvPhrase){
		this.csvClick = true;
		this.sideEnjeux = false;
		this.csvPhrase = csv;
		this.csvSelected = csv.nom;
		let self = this;
		// if(self.phrases[csv.nom] == undefined){
		// 	let ref = self.db.object("phrases_csv/"+csv.nom).snapshotChanges().subscribe(function(phrasesSnap){
		// 		ref.unsubscribe();
		// 		let phrases:Phrase[] = [];
		// 		phrasesSnap.payload.forEach(function(child){
		// 			phrases.push(new Phrase().deserialize(child.val()));
		// 			return false;
		// 		});

		// 		self.csvPhrase.phrases = phrases.filter(function(value:Phrase, index:number, array:Phrase[]){
	 //            let perf = self.getMaxPerfPhrase(value).perf;
	 //            let ok = false;
	 //            if(self.selectedEnjeux.length > 0){
	 //            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);
		// 			if(check != undefined){
		// 				ok = true;
		// 			}
	 //            }else{
	 //            	ok = true;
	 //            }


	 //            return (perf >= self.seuil) && ok;

	 //          })
		// 	});
		// }else{
		// 	self.csvPhrase.phrases = self.phrases[csv.nom].filter(function(value:Phrase, index:number, array:Phrase[]){
	 //            let perf = self.getMaxPerfPhrase(value).perf;
	 //            let ok = false;
	 //            if(self.selectedEnjeux.length > 0){
	 //            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);

		// 			if(check != undefined){
		// 				ok = true;
		// 			}
	 //            }else{
	 //            	ok = true;
	 //            }


	 //            return (perf >= self.seuil) && ok;

	 //          });
		// }





			self.collapsed2 = false;
	        self.collapsedEvent.emit(self.collapsed2);




	}

	selectTab(type:string){
		switch (type) {
			case "map":
				this.mapToogle = true;
				this.hide_graph = true;
				this.hide_map = false;
				break;
			case "chart":
				this.mapToogle = false;
				this.hide_graph = false;
				this.hide_map = true;

				break;
			default:
				// code...
				break;
		}

		this.checkFilter();



		// console.log("mapToogle %o", this.mapToogle);
	}

	toogleFluxLimit(){
		if(this.fluxLimit == 3){
			this.fluxLimit = this.flux.length;
		}else{
			this.fluxLimit = 3;
		}
	}

	fluxColor(f:any){

		let style:any = {
			"color" : this.getColor(f.flux),
			"font-size": "0.8em"
		}

		return style;

	}

	infoDetailStyle(){
		let h = jQuery(".info-csv").height();
		let nh:number = 590 - h;
		let style = {
			"height" : nh.toString()+"px",
			"overflow-y" : "scroll"
		}
		return style;
	}

	addEnjeuxStyle(){
		let style = {
			"padding-left" : "57px",
			"overflow" : "scroll"

		};
		if(this.countEnjeu < 4){
			style["max-height"] = "75%";
		}else if(this.countEnjeu < 8){
			style["max-height"] = "65%";
		}else if(this.countEnjeu < 12){
			style["max-height"] = "56%";
		}else if(this.countEnjeu >= 12){
			style["max-height"] = "47%";
		}
		return style;


	}
	phraseStyle(sat){
		if(sat == "0"){
			sat = "1";
		}
		let style = {
			"border-left": "5px solid "+this.couleurPhrase[sat],
			"border-radius": "3px",
			"margin-top" : "20px"
		}
		return style;
	}

	makerStyle(csv:CsvPhrase){

		let color = "#eee";



			color = this.getColor(csv.flux);


		let style:any = {
			"color": color,
			"fill": color
		}

		// if(csv.nom == this.csvSelected){
		// 	style = {
		// 		"color": color,
		// 		"fill": color,
		// 		"height": "34px !important",
		// 		"width": "34px !important",
		// 		"position": "relative !important",
		// 		"top": "-14px !important",
		// 	}

		// }

		return style;
	}

	lang(mot:string){
		mot = mot.toLowerCase().replace(/\s+$/, '');

		return this.traduction[mot] != undefined ? this.traduction[mot][this.translate.currentLang] : mot;
	}

	fr(mot){
	      return this.traduction[mot] != undefined ? this.traduction[mot]['fr'] : mot;
	    }

	en(mot){
	      return this.traduction[mot] != undefined ? this.traduction[mot]['en'] : mot;
	}

	getColor(text:string){
		text = text.toLowerCase().trim();
		return this.couleurs ? (this.couleurs[text] ? this.couleurs[text] : '#868E96') : '#868E96';
	}

	styleLegend(text){
		let style = {
			"font-size" : "0.8em",
			"color" : this.getColor(text)
		}
		return style;
	}

	getColorIcon(text:string){

		text = text.toLowerCase();
		return this.couleurs ? (this.couleurs[text] ? this.couleurs[text] : '#868E96') : '#868E96';
	}

  	handleSVG(svg: SVGElement, parent: Element | null): SVGElement {
	    // console.log('Loaded SVG: ', svg.childNodes, parent);
	    svg.setAttribute('fill', 'white');

	    // svg.firstChild.textContent = svg.firstChild.textContent.replace(/000000/g, "ff0000");
	    // svg.firstChild.textContent = svg.firstChild.textContent.replace(/000/g, "ff0000");
	    // console.log("after %o",svg.firstChild.textContent);
	 //    svg.childNodes.forEach(function(item){
	 //    	if(item.type){
	 //    		item.setAttribute('fill', 'white');
	 //    		item.setAttribute('stroke', 'white');
	 //    	}
	 //    	console.log(item.type);
		//     // item.setAttribute('fill', 'white');
		// });
	    return svg;
	}


	getMaxPerfPhrase(phrase:Phrase){
	    // console.log("getting max perf");

	    let enjeu:Enjeu;
	    let perf = 0;
	    let perfSat = 0;

	    for(let i in this.enjeux){

	      let cur = this.computePerf(phrase, this.enjeux[i]);

	      if(cur > perf){
	        enjeu = this.enjeux[i];
	        perf = cur;
	      }
	    }

	    // perfSat = this.computeSatPerf(phrase).points;

	    return {"perf": perf, "enjeu": enjeu, "perfSat": perfSat};
	}

	computePerf(phrase:Phrase, enjeu:Enjeu){
	    //mettre phrase en minuscule
	    if(phrase){
	        let phrase_fr = " "+phrase.phrase_fr.toLowerCase()+" ";
	        let phrase_en = " "+phrase.phrase_en.toLowerCase()+" ";

	        let perf_fr = 0;
	        let perf_en = 0;
	        //fr
	        if(phrase.langue.toLowerCase() == "fr"){
	        	for(let i in enjeu.mots.fr){
		          let reg = new RegExp("\\s" + enjeu.mots.fr[i].mot.toLowerCase() + "\\s");
		          if(phrase_fr.search(reg) > 0){
		            perf_fr += enjeu.mots.fr[i].point;

		          }
		        }
	        }else{
	        	//en
		        for(let i in enjeu.mots.en){
		          let reg = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s");
		          if(phrase_en.search(reg) > 0){
		            perf_en += enjeu.mots.en[i].point;

		          }
		        }
	        }




	        return (perf_fr > perf_en) ? perf_fr : perf_en;
	    }else{
	      return 0;
	    }


	}

	computeCsvSat(phrases:Phrase[], enjeu:Enjeu){
		let sat = [];
		for(let p in phrases){
			let phrase = phrases[p];

			if(phrase.enjeu == enjeu.num){

				if(sat[phrase.satisfaction] == undefined){
					sat[phrase.satisfaction] = 1;
				}else{
					sat[phrase.satisfaction] += 1;
				}
			}
		}
		let res = "1";
		let cur = 0;
		for(let i in sat){
			if(sat[i] > cur){
				cur = sat[i];
				res = i;
			}
		}

		return res;
	}

	open(content) {
        this.modalService.open(content, {size: 'lg', centered:true}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

	alert(text){
		alert(text);
	}


	receiveCollapsed($event) {
        this.collapedSideBar = $event;
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    eventCalled2() {
        this.isActive2 = !this.isActive2;
    }

    eventCalled3() {
        this.isActive3 = !this.isActive3;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    toggleCollapsed() {
    	this.csvClick = false;
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    }

    toggleCollapsed2() {
    	this.csvClick = false;
        this.collapsed2 = !this.collapsed2;
        this.collapsedEvent.emit(this.collapsed2);
    }

    toggleCollapsed3(){
    	this.sideEnjeux = true;
        this.collapsed3 = false;
        this.collapsed = true;
        this.collapsedEvent.emit(this.collapsed3);
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    isToggledEnjeu(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushLeftClass);
    }

    toggleSidebar() {
    	const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }
    toggleSidebar2() {
    	this.csvClick = false;
    	this.sideEnjeux = false;
    	this.collapsed3 = true;
        this.collapsed = false;
        this.collapsedEvent.emit(this.collapsed);
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    toggleSidebar3(){
    	//this.csvClick = true;
    	this.sideEnjeux = true;
    	const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    toggleSidebarEnjeu() {
    	this.collapsed3 = false;
	    this.collapsedEvent.emit(this.collapsed3);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    checkEnjeu(idEnjeu,client_enjeu){
    	let self = this;
    	let test = true;

    	for(let i in client_enjeu){
    		if(test){
    			if(client_enjeu[i].key == idEnjeu.toString()){
	    			test = false;
	    			break;
	    		}
    		}
    	}
    	return test;
    }

    testvalue(def){
    	let test = true;
    	if(test){
    		if(def==0){

	    		test = false;
	    	}
    	}
    	return test;
	}

	showCheckboxes(){
		if(this.showCheck){
			this.showCheck = false;
		} else {
			this.showCheck = true;
		}
	}

	deplace_left(csv){

		let self = this;

	   	for(let i=0;i<this.csvPhrases.length;i++){
	   		if(self.csvPhrases[i].nom == csv.nom){
	   			let sum = i+1;
	   			//let min = i-1;
   				// if(self.csvPhrases[sum] == undefined){
   				// 	this.selectCsvPhrase2(self.csvPhrases[min]);
   				// } else if(self.csvPhrases[min] == undefined){
   				// 	this.selectCsvPhrase2(self.csvPhrases[sum]);
   				// }/* else {
   				// 	this.toggleCollapsed2();
   				// }*/

   				if(self.csvPhrases[sum] != undefined){
   					this.csvPhrase = self.csvPhrases[sum];
   					this.selectCsvPhrase2(self.csvPhrases[sum]);
   				}
	   		}
	   	}
	}

	deplace_right(csv){
		let self = this;
		let test2 = true;
	   	for(let i=0;i<this.csvPhrases.length;i++){
	   		if(self.csvPhrases[i].nom == csv.nom){
	   			let min = i-1;
   				if(self.csvPhrases[min] != undefined){
   					this.csvPhrase = self.csvPhrases[min];
   					this.selectCsvPhrase2(self.csvPhrases[min]);
   				}
	   		}
	   	}
	}
}
