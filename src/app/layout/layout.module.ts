import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './home/home.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { environment } from '../../environments/environment';

import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';

import { NgxLoadingModule } from 'ngx-loading';
import { NgxAsideModule } from 'ngx-aside';
import { FormsModule} from '@angular/forms';

 import { NgxEchartsModule } from 'ngx-echarts';

 import { AngularFireAuthModule } from '@angular/fire/auth';

 import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

 import { Ng5SliderModule } from 'ng5-slider';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 


@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule,
        NgxMapboxGLModule.withConfig({
          accessToken: environment.mapbox.accessToken
        }),
        HttpClientModule, InlineSVGModule.forRoot(),
        NgxLoadingModule,
        NgxAsideModule,
        FormsModule,
        NgxEchartsModule,
        AngularFireAuthModule,
        NgbModalModule,
        Ng5SliderModule
        // BrowserAnimationsModule
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, HomeComponent]
})
export class LayoutModule {}
